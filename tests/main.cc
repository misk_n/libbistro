#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>

/*
 * Headers for the example
*/

#include <cstdint>

#include "base.hh"
#include "bignum.hh"
#include <cassert>
#include "ast-factory.hh"

int main()
{
  using value_t = uint32_t;
  using base_t = bistro::Base<value_t>;
  using bignum_t = bistro::BigNum<value_t>;

  auto b = bignum_t(10);
  b.set_digit(3, 2);
  std::cout << b.get_digit(2) << "\n"
            << b.get_digit(3) << "\n"
            << b.get_num_digits() << "\n";
  b.set_digit(3, 0);
  std::cout << b.get_num_digits() << "\n";
  try
  {
    b.get_digit(0);
  }
  catch (const std::out_of_range& e)
  {
    std::cerr << "Out of range: " << e.what() << '\n';
  }
  auto base = base_t{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};

  std::ifstream ss("num.tes");

  auto bptr = std::make_shared<bignum_t>(ss, base);
/*
  auto num = std::make_shared<bistro::NumberNode<bignum_t, base_t>>(bptr);
  auto minus = std::make_shared<bistro::UnOpNode<bignum_t, base_t>>(num,
                bistro::UnOpType::MINUS);
  auto plus = bistro::BinOpNode<bignum_t, base_t>(num, minus,
                bistro::BinOpType::PLUS);
  plus.print_infix(std::cout, base) << '\n';
  plus.print_pol(std::cout, base) << '\n';
  plus.eval()->print(std::cout, base) << '\n';
*/
  std::ifstream in("tst_file");
  auto factory = bistro::ASTFactory<bignum_t, base_t>{};
  auto ast_base = factory.read(in);
  ast_base.first->print_infix(std::cout, ast_base.second) << '\n';
  ast_base.first->eval()->print(std::cout, ast_base.second) << '\n';

  return 0;
}
