namespace bistro
{
  template <typename Value, typename Char>
  std::size_t Base<Value, Char>::get_base_num() const
  {
    return character_->size();
  }

  template <typename Value, typename Char>
  void Base<Value, Char>::add_digit(char_t repr)
  {
    if (is_digit(repr) || is_operator(repr))
      throw std::invalid_argument(
        "representation is already defined or is an operator");
    character_->push_back(repr);
    (*values_)[repr] = values_->size();
  }

  template <typename Value, typename Char>
  bool Base<Value, Char>::is_digit(Char c) const
  {
    auto res = values_->find(c);
    return res != values_->end();
  }

  template <typename Value, typename Char>
  bool Base<Value, Char>::is_operator(Char c)
  {
    return (c == '=' ||
            c == '+' ||
            c == '*' ||
            c == '-' ||
            c == '/' ||
            c == '%' ||
            c == '(' ||
            c == ')');
  }

  template <typename Value, typename Char>
  typename Base<Value, Char>::
  char_t Base<Value, Char>::get_digit_representation(value_t i) const
  {
    if (i >= character_->size())
      throw std::out_of_range("digit is greater than the base");
    return (*character_)[i];
  }

  template <typename Value, typename Char>
  Value Base<Value, Char>::get_char_value(Char r) const
  {
    auto res = values_->find(r);
    if (res == values_->end())
      throw std::out_of_range("no digit associated to this representation");
    return res->second - 1;
  }
}
