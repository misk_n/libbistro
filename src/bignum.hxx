#include <list>

namespace bistro
{
  template <typename T>
  BigNum<T>::BigNum(std::size_t base)
  {
    num_base_ = base;
    d_container_ = std::make_shared<std::vector<digit_t>>();
    this->set_positive(true);
  }

  template <typename T>
  template <typename Base>
  BigNum<T>::BigNum(std::ifstream& in, const Base& b)
  {
    num_base_ = b.get_base_num();
    d_container_ = std::make_shared<std::vector<digit_t>>();
    digit_t c = in.get();
    auto l = std::list<digit_t>();
    while(true)
    {
      while (c == 10)
        c = in.get();
      if (b.is_digit(c))
        l.push_front(b.get_char_value(c));
      else
      {
        in.unget();
        break;
      }
      c = in.get();
    }
    if (!l.size())
      throw std::length_error("can't create a BigNum : \
no digit at the given position in the stream");
    index_t i = 0;
    for (auto it = l.begin(); it != l.end(); it++)
    {
      set_digit(i, *it);
      i++;
    }
    this->set_positive(true);
  }

  template <typename T>
  BigNum<T> BigNum<T>::clone() const
  {
    self_t res = BigNum<digit_t>(this->num_base_);
    res.set_positive(this->is_positive());
    res.d_container_ = this->d_container_;
    return res;
  }
}

#include "bignum/bignum_accessors.hxx"
#include "bignum/bignum_comparisons.hxx"
#include "bignum/bignum_printer.hxx"
#include "bignum/bignum_operators.hxx"
