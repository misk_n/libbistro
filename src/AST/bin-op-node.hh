#pragma once

#include <memory>
#include <ostream>

namespace bistro
{
  template <typename BigNum, typename Base>
  class BinOpNode : public ASTNode<BigNum, Base>
  {
  public:

    ~BinOpNode() override
      {
      }

    BinOpNode(char c,
              std::shared_ptr<ASTNode<BigNum, Base>> left,
              std::shared_ptr<ASTNode<BigNum, Base>> right)
      {
        this->set_op(c);
        this->set_left(left);
        this->set_right(right);
      }

    std::ostream& print_infix(std::ostream& out,
                              const Base& b) const override
      {
        out << '(';
        left_->print_infix(out, b);
        out << op_;
        right_->print_infix(out, b);
        out << ')';
        return out;
      }


    std::shared_ptr<BigNum> eval() const override
      {
        BigNum a = left_->eval()->clone();
        BigNum b = right_->eval()->clone();
        auto res = std::make_shared<BigNum>(10);
        if (op_ == '+')
          *res = a + b;
        if (op_ == '-')
          *res = a - b;
        if (op_ == '*')
          *res = a * b;
        if (op_ == '/')
          *res = a / b;
        if (op_ == '%')
          *res = a % b;
        return res;
      }

    void set_op(char c)
      {
        op_ = c;
      }

    void set_left(std::shared_ptr<ASTNode<BigNum, Base>> n)
      {
        left_ = n;
      }

    void set_right(std::shared_ptr<ASTNode<BigNum, Base>> n)
      {
        right_ = n;
      }

    std::ostream& print_pol(std::ostream& out, const Base& b) const override
      {
        out << op_;
        out << ' ';
        left_->print_pol(out, b);
        out << ' ';
        right_->print_pol(out, b);
        return out;
      }

    std::ostream& print_rpol(std::ostream& out, const Base& b) const override
      {
        left_->print_rpol(out, b);
        out << ' ';
        right_->print_rpol(out, b);
        out << ' ';
        out << op_;
        return out;
      }

  private:
    char op_;
    std::shared_ptr<ASTNode<BigNum, Base>> left_;
    std::shared_ptr<ASTNode<BigNum, Base>> right_;
  };
}
