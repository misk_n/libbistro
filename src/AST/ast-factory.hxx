#include "bin-op-node.hh"
#include "number-node.hh"
#include "un-op-node.hh"

namespace bistro
{
  static bool look_ahead(std::ifstream& in, char c)
  {
    char tmp = in.get();
    in.unget();
    return tmp == c;
  }

  template<typename BigNum, typename Base>
  std::pair<std::shared_ptr<ASTNode<BigNum, Base>>, Base>
  ASTFactory<BigNum, Base>::read(std::ifstream& in)
  {
    base_t b = read_base(in);
    node_t n = read_AST(in, b);
    auto res = std::pair<node_t, base_t>(n, b);
    return res;
  }

  template<typename BigNum, typename Base>
  Base ASTFactory<BigNum, Base>::read_base(std::ifstream& in)
  {
    unsigned base;
    in >> base;
    if (base == 0)
      throw std::domain_error("invalid base format");
    std::string line("");
    in >> line;
    if (line == "")
      throw std::domain_error("invalid base format");
    auto l = std::vector<char>();
    for (auto it = line.begin(); it != line.end(); it++)
      l.push_back(*it);
    auto b = Base();
    for (auto it = l.begin(); it != l.end(); it++)
      b.add_digit(*it);
    return b;
  }

  template<typename BigNum, typename Base>
  std::shared_ptr<ASTNode<BigNum, Base>>
    recognize_Exp(std::ifstream& in, const Base& b);

  template<typename BigNum, typename Base>
  std::shared_ptr<ASTNode<BigNum, Base>>
    recognize_F(std::ifstream& in, const Base& b)
  {
    if (!in.good())
      throw std::domain_error("unexpected EOF, expected '+', '-', '(' or\
 number");
    auto pos = in.tellg();
    char c;
    in >> c;
    if (c == '+' || c == '-')
    {
      auto f = recognize_F<BigNum, Base>(in, b);
      return std::make_shared<UnOpNode<BigNum, Base>>(c, f);
    }
    if (c == '(')
    {
      auto exp = recognize_Exp<BigNum, Base>(in, b);
      if (!in.good())
        throw std::domain_error("unexpected EOF, expected ')'");
      in >> c;
      if (c != ')')
        throw std::domain_error(std::string("unexpected '")
                                + c
                                + std::string("', expected ')'"));
      return exp;
    }
    in.seekg(pos);
    auto ptr = std::make_shared<BigNum>(in, b);
    auto res = std::make_shared<NumberNode<BigNum, Base>>(ptr);
    return res;
  }

  template<typename BigNum, typename Base>
  std::shared_ptr<ASTNode<BigNum, Base>>
    recognize_T(std::ifstream& in, const Base& b)
  {
    if (!in.good())
      throw std::domain_error("unexpected EOF, expected F");
    auto left = recognize_F<BigNum, Base>(in, b);
    if (!(look_ahead(in, '*')
          || look_ahead(in, '/')
          || look_ahead(in, '%')))
      return left;
    char c;
    in >> c;
    if (c != '*' && c != '/' && c != '%' && c != ')')
      throw std::domain_error(std::string("unexpected '") +
                              c +
                              std::string("', expected '*', '/' or '%'"));
    auto right = recognize_T<BigNum, Base>(in, b);
    return std::make_shared<BinOpNode<BigNum, Base>>(c, left, right);
  }

  template<typename BigNum, typename Base>
  std::shared_ptr<ASTNode<BigNum, Base>>
   recognize_Exp(std::ifstream& in, const Base& b)
  {
    if (!in.good())
      throw std::domain_error("unexpected EOF, epected T");
    auto left = recognize_T<BigNum, Base>(in, b);
    if (!(look_ahead(in, '+')
          || look_ahead(in, '-')))
      return left;
    char c;
    in >> c;
    if (c != '+' && c != '-')
      throw std::domain_error(std::string("unexpected '")
                              + c
                              + std::string("', expected '+' or '-'"));
    auto right = recognize_Exp<BigNum, Base>(in, b);
    return std::make_shared<BinOpNode<BigNum, Base>>(c, left, right);
  }

  template <typename BigNum, typename Base>
  std::shared_ptr<ASTNode<BigNum, Base>>
  ASTFactory<BigNum, Base>::read_AST(std::ifstream& in, const base_t& b)
  {
    node_t ast = recognize_Exp<BigNum, Base>(in, b);
    return ast;
  }
}
