#pragma once

namespace bistro
{
  template <typename BigNum, typename Base>
  class NumberNode : public ASTNode<BigNum, Base>
  {
    using base_t = Base;
    using num_t = std::shared_ptr<BigNum>;
  public:
    ~NumberNode() override
      {
      }

    NumberNode(num_t number)
      {
        number_ = number;
      }

    std::ostream& print_infix(std::ostream& out, const base_t& b) const override
      {
        number_->print(out, b);
        return out;
      }

    std::ostream& print_pol(std::ostream& out, const base_t& b) const override
      {
        number_->print(out, b);
        return out;
      }

    std::ostream& print_rpol(std::ostream& out, const base_t& b) const override
      {
        number_->print(out, b);
        return out;
      }

    num_t eval() const override
      {
        return number_;
      }

    void set_number(num_t n)
      {
        number_ = n;
      }

  private:
    num_t number_;
  };
}
