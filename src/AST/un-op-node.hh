#pragma once

#include "ast-node.hh"

namespace bistro
{
  template <typename BigNum, typename Base>
  class UnOpNode : public ASTNode<BigNum, Base>
  {
  public:

    using base_t = Base;
    using num_t = std::shared_ptr<BigNum>;
    using self_t = ASTNode<BigNum, Base>;
    using node_t = std::shared_ptr<self_t>;

    ~UnOpNode() override
      {
      }

    UnOpNode(char c, node_t child)
      {
        this->set_sign(c);
        this->set_child(child);
      }

    std::ostream& print_infix(std::ostream& out, const base_t& b) const override
    {
      if (sign_)
        out << '+';
      else
        out << '-';
      return child_->print_infix(out, b);
    }

    num_t eval() const override
    {
      num_t child_res = child_->eval();
      if (!sign_)
        child_res->set_positive(!(child_res->is_positive()));
      return child_res;
    }

    void set_sign(char c)
      {
        sign_ = c == '+';
      }

    void set_child(node_t child)
      {
        child_ = child;
      }

    std::ostream& print_pol(std::ostream& out, const base_t& b) const override
      {
        out << (sign_ ? '+' : '-');
        out << " 0 ";
        child_->print_infix(out, b);
        return out;
      }

    std::ostream& print_rpol(std::ostream& out, const base_t& b) const override
      {
        out << "0 ";
        child_->print_infix(out, b);
        out << ' ';
        out << (sign_ ? '+' : '-');
        return out;
      }

  private:
    bool sign_;
    node_t child_;
  };
}
