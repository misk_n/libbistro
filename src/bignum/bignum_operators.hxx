namespace bistro
{
  template<typename T>
  static T get_math_pos(std::shared_ptr<std::vector<T>> container, std::size_t i)
  {
    if (i >= container->size())
      return 0;
    else
      return (*container)[i];
  }

  template<typename T>
  BigNum<T> BigNum<T>::operator+(const BigNum<T>& other) const
  {
    using digit_t = T;
    if (num_base_ != other.num_base_)
      throw std::invalid_argument("Addition between number of different bases");
    if (this->is_positive() && other.is_positive())
    {
      digit_t r = 0;
      std::size_t base = num_base_;
      auto res = BigNum<T>(base);
      std::size_t a = d_container_->size();
      std::size_t b = other.d_container_->size();
      std::size_t pos = 0;
      while (a > 0 || b > 0 || r)
      {
        digit_t d1 = get_math_pos<digit_t>(d_container_, pos);
        digit_t d2 = get_math_pos<digit_t>(other.d_container_, pos);
        digit_t local = r + d1 + d2;
        r = local / base;
        res.set_digit(pos, local - r * base);
        pos++;
        if (a)
          a--;
        if (b)
          b--;
      }
      return res;
    }
    if (other.is_positive())
    {
      auto cpy = this->clone();
      cpy.set_positive(true);
      auto res = other - cpy;
      return res;
    }
    auto cpy = other.clone();
    cpy.set_positive(true);
    auto res = *this - cpy;
    return res;
  }

  template<typename T>
  BigNum<T> BigNum<T>::operator-(const BigNum<T>& other) const
  {
    using digit_t = T;
    if (num_base_ != other.num_base_)
      throw std::invalid_argument("Addition between number of different bases");
    if (this->is_positive() && other.is_positive())
    {
      if (other > *this)
      {
        auto res = other - *this;
        res.set_positive(false);
        return res;
      }
      digit_t r = 0;
      std::size_t base = num_base_;
      auto res = BigNum<T>(base);
      std::size_t a = d_container_->size();
      std::size_t b = other.d_container_->size();
      std::size_t pos = 0;
      while (a > 0 || b > 0 || r)
      {
        digit_t d1 = get_math_pos<digit_t>(d_container_, pos);
        digit_t d2 = get_math_pos<digit_t>(other.d_container_, pos) + r;
        if (d1 < d2)
        {
          r = 1;
          d1 += base;
        }
        else
          r = 0;
        res.set_digit(pos, d1 - d2);
        pos++;
        if (a)
          a--;
        if (b)
          b--;
      }
      return res;
    }
    if (other.is_positive())
    {
      auto cpy = this->clone();
      cpy.set_positive(true);
      auto res = cpy + other;
      res.set_positive(false);
      return res;
    }
    auto cpy = other.clone();
    cpy.set_positive(true);
    auto res = *this + cpy;
    return res;
  }

  template<typename T>
  BigNum<T>& BigNum<T>::operator+=(const BigNum<T>& other)
  {
    auto local = *this + other;
    this->d_container_ = local.d_container_;
    this->positive_ = local.positive_;
    this->num_base_ = local.num_base_;
    return *this;
  }

  template<typename T>
  BigNum<T>& BigNum<T>::operator-=(const BigNum<T>& other)
  {
    auto local = *this - other;
    this->d_container_ = local.d_container_;
    this->positive_ = local.positive_;
    this->num_base_ = local.num_base_;
    return *this;
  }

  template<typename T>
  BigNum<T> BigNum<T>::operator*(const BigNum<T>& other) const
  {
    using digit_t = T;
    if (num_base_ != other.num_base_)
      throw std::invalid_argument("Addition between number of different bases");
    std::size_t base = num_base_;
    auto res = BigNum<T>(base);
    if (!this->d_container_->size() || !other.d_container_->size())
      return res;
    for (std::size_t start = 0; start < other.d_container_->size(); start++)
    {
      digit_t r = 0;
      digit_t m = (*other.d_container_)[start];
      std::size_t pos = start;
      auto local = BigNum<T>(base);
      for (auto it = d_container_->begin(); it != d_container_->end(); it++)
      {
        digit_t u = m * *it + r;
        r = u / base;
        u = u % base;
        local.set_digit(pos, u);
        pos++;
      }
      local.set_digit(pos, r);
      res += local;
    }
    return res;
  }

  template<typename T>
  BigNum<T>& BigNum<T>::operator*=(const BigNum<T>& other)
  {
    auto local = *this * other;
    this->d_container_ = local.d_container_;
    this->positive_ = local.positive_;
    this->num_base_ = local.num_base_;
    return *this;
  }

  template<typename T>
  BigNum<T> BigNum<T>::operator/(const BigNum<T>& other) const
  {
    if (!other.d_container_->size())
      throw std::overflow_error("division by zero");
    if (other.is_positive() && this->is_positive())
    {
      auto q = BigNum<T>(num_base_);
      auto increment = BigNum<T>(num_base_);
      auto reste = *this + increment;
      increment.set_digit(0, 1);
      do
      {
        q += increment;
        reste -= other;
      } while (reste.is_positive());
      return q - increment;
    }
    auto a = this->clone();
    auto b = other.clone();
    a.set_positive(true);
    b.set_positive(true);
    auto res = a / b;
    res.set_positive(this->is_positive() == other.is_positive());
    return res;
  }

  template<typename T>
  BigNum<T>& BigNum<T>::operator/=(const BigNum<T>& other)
  {
    auto local = *this / other;
    this->d_container_ = local.d_container_;
    this->positive_ = local.positive_;
    this->num_base_ = local.num_base_;
    return *this;
  }


  template<typename T>
  BigNum<T> BigNum<T>::operator%(const BigNum<T>& other) const
  {
    if (!other.d_container_->size())
      throw std::overflow_error("division by zero");
    if (other.is_positive() && this->is_positive())
    {
      auto zero = BigNum<T>(num_base_);
      auto reste = *this + zero;
      do
      {
        reste -= other;
      } while (reste.is_positive());
      return reste + other;
    }
    auto a = this->clone();
    auto b = other.clone();
    a.set_positive(true);
    b.set_positive(true);
    auto res = a % b;
    res.set_positive(this->is_positive() == other.is_positive());
    return res;
  }

  template<typename T>
  BigNum<T>& BigNum<T>::operator%=(const BigNum<T>& other)
  {
    auto local = *this % other;
    this->d_container_ = local.d_container_;
    this->positive_ = local.positive_;
    this->num_base_ = local.num_base_;
    return *this;
  }

}
