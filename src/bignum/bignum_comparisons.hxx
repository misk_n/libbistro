namespace bistro
{
  template<typename T>
  bool BigNum<T>::operator>(const self_t& other) const
  {
    if (this->positive_ != other.positive_)
      return this->positive_;
    if (other.d_container_->size() != d_container_->size())
      return d_container_->size() > other.d_container_->size();
    for (auto it1 = d_container_->rbegin(),
           it2 = other.d_container_->rbegin();
         it1 != d_container_->rend();
         it1++)
    {
      if (*it1 != *it2)
        return this->positive_ ? *it1 > *it2 : *it1 < *it2;
      it2++;
    }
    return false;
  }

  template<typename T>
  bool BigNum<T>::operator==(const self_t& other) const
  {
    if (this->positive_ != other.positive_)
      return false;
    if (other.d_container_->size() != d_container_->size())
      return false;
    for (auto it1 = d_container_->begin(),
           it2 = other.d_container_->begin();
         it1 != d_container_->end();
         it1++)
    {
      if (*it1 != *it2)
        return false;
      it2++;
    }
    return true;
  }

  template<typename T>
  BigNum<T>::operator bool() const
  {
    return !d_container_->size();
  }

}
