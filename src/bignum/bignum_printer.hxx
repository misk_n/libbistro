namespace bistro
{
  template <typename T>
  template<typename Base>
  std::ostream& BigNum<T>::print(std::ostream& out, const Base& b) const
  {
    if (b.get_base_num() != num_base_)
      throw std::invalid_argument("invalid base to represent this BigNum");
    if (!positive_)
      out << '-';
    if (!d_container_->size())
      out << '0';
    for (auto it = d_container_->rbegin(); it < d_container_->rend(); it++)
    {
      out << b.get_digit_representation(*it);
    }
    return out;
  }
}
