namespace bistro
{
  template<typename T>
  std::size_t BigNum<T>::get_num_digits() const
  {
    return d_container_->size();
  }

  template<typename T>
  T BigNum<T>::get_digit(index_t i) const
  {
    if (d_container_->size() <= i)
      throw std::out_of_range("index superior to bignum size");
    return (*d_container_)[i];
  }

  template<typename T>
  void BigNum<T>::set_digit(index_t i, digit_t d)
  {
    if (d >= num_base_)
      throw std::invalid_argument("digit value superior to given base");
    while (d_container_->size() <= i && d != 0)
      d_container_->push_back(0);
    if (d_container_->size() <= i)
      return;
    (*d_container_)[i] = d;
  }

  template<typename T>
  bool BigNum<T>::is_positive() const
  {
    return positive_;
  }

  template<typename T>
  void BigNum<T>::set_positive(bool p)
  {
    positive_ = p;
  }
}
