#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>

/*
 * Headers for the example
*/

#include <cstdint>

#include "base.hh"
#include "bignum.hh"
#include <cassert>
#include "ast-factory.hh"

int main(int argc, char *argv[])
{
  using bignum_t = bistro::BigNum<unsigned>;
  using base_t = bistro::Base<unsigned>;
  if (argc != 2)
    return 2;
  std::ifstream in(argv[1]);
  auto factory = bistro::ASTFactory<bignum_t, base_t>{};
  auto ast_base = factory.read(in);
#ifdef DEBUG
  ast_base.first->print_infix(std::cout, ast_base.second) << '\n';
  ast_base.first->print_pol(std::cout, ast_base.second) << '\n';
  ast_base.first->print_rpol(std::cout, ast_base.second) << '\n';
#endif
  ast_base.first->eval()->print(std::cout, ast_base.second) << '\n';

  return 0;
}
